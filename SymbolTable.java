import java.util.*;
import java.util.regex.Pattern;
import java.io.*;
class symbol																//class for creating symbol table
{
	static ArrayList<String[]> table ;
	static ArrayList<String> opcode ;
	static ArrayList<String> variable ;
	static ArrayList<String> label ;
	
	public symbol()
	{
		table = new ArrayList<String[]>();
		opcode = new ArrayList<String>();
		label = new ArrayList<String>();
		variable = new ArrayList<String>();
		opcode.add("CLA");
		opcode.add("LAC");
		opcode.add("SAC");
		opcode.add("ADD");
		opcode.add("SUB");
		opcode.add("BRZ");
		opcode.add("BRN");
		opcode.add("BRP");
		opcode.add("INP");
		opcode.add("DSP");
		opcode.add("MUL");
		opcode.add("DIV");
		opcode.add("STP");
		opcode.add("DS");
		opcode.add("DW");
		opcode.add("DB");
		opcode.add("DQ");
		opcode.add("DC");
	}
	public void add(String[] list)
	{
		table.add(list);
	}
	public void display_table()
	{
		String ans = "";
		//SYMBOL TABLE formatting done to be printed on console
		System.out.println("\t\t\t\t\tSYMBOL TABLE");
		System.out.println("------------------------------------------------------------------------------------------------------------");
		System.out.println("SYMBOL\t\t\tTYPE\t\t\tILC\t\t\tVALUE\t\t\tSIZE");
		System.out.println("------------------------------------------------------------------------------------------------------------");
		for(String[] arr : table)
		{
			if(arr[1].equals("Label"))
			{
				arr[3]="";
				arr[4]="";
				ans = arr[0]+"\t\t\t"+arr[1]+"\t\t\t"+arr[2]+"\t\t\t"+arr[3]+"\t\t\t"+arr[4];
			}
			else
				ans = arr[0]+"\t\t\t"+arr[1]+"\t\t"+arr[2]+"\t\t\t"+arr[3]+"\t\t\t"+arr[4];
			System.out.println(ans);
		}
	}
	public void final_write()												//function to write generated symbol table to another file
	{
		try
		{
			File file = new File("out.txt");
			FileOutputStream fo = new FileOutputStream(file);
			byte[] b ;
			String ans;
			for(String[] arr : table)
			{
				ans = "";
				ans = arr[0]+""+arr[1]+" "+arr[2]+" "+arr[3]+" "+arr[4];
				b = ans.getBytes();
				for(byte a : b)
				{
					fo.write(a);
				}
			}
			
			fo.close();
		}
		catch(Exception e)
		{
			System.out.println("Unable to Open File in Write Mode");
		}
	}
}
class symbol_table extends symbol
{
	private static final boolean String = false;
	static boolean SYMERROR=false;
	static long ilc;
	private long n ;
	private ArrayList<String> arr ;
	public symbol_table()
	{
		symbol_table.setIlc(0);
		this.setN(count_number_of_lines());
		this.setArr(read());
		
	}
	
	public ArrayList<String> getArr() {
		return arr;
	}

	public void setArr(ArrayList<String> arr) {
		this.arr = arr;
	}

	public long count_number_of_lines()										//function to count number of lines in input file
	{
		long ans = 0;
		try
		{
			FileInputStream fo = new FileInputStream("finalinput.txt");
			int n = fo.available();
			byte k;
			while(n--!=0)
			{
				k = (byte) fo.read();
				if(k==10) 
				{
					ans++;
				}
			}
			fo.close();
		}
		catch(Exception e)
		{
			System.out.println("Unable to Open File in Write Mode");
		}
		return ++ans;
	}
	
	public static long getIlc() 
	{
		return ilc;
	}

	public static void setIlc(long ilc) 
	{
		symbol_table.ilc = ilc;
	}

	public long getN() {
		return n;
	}

	public void setN(long n) 
	{
		this.n = n;
	}

	public void write(String str)
	{
		try
		{
			FileOutputStream fo = new FileOutputStream("finalinput.txt");
			byte[] b = str.getBytes();
			for(byte a : b)
			{
				fo.write(a);
			}
			fo.close();
		}
		catch(Exception e)
		{
			System.out.println("Unable to Open File in Write Mode");
		}
	}
	
	public ArrayList<String> read()											//function to read input file 
	{
		ArrayList<String> ans = new ArrayList<String>();
		String str = "";
		long ind = 0;
		BufferedReader reader;
		try
		{
			FileInputStream fstream = new FileInputStream("finalinput.txt");
			reader = new BufferedReader(new InputStreamReader(fstream));
	         String line=reader.readLine();
	         int i=0;
	         while(line!=null)
	         {
	        	 	try
	        	 	{
	        	 		int spaces=MainCode.countspaces(line);
	        	 		if(spaces>2)
	        	 			throw(new MoreThanThreeFields());
	        	 	}
	        	 	catch(MoreThanThreeFields e)
	        	 	{
	        	 		SYMERROR=true;
	    				System.out.println(e.getMessage()+" AT LINE "+i+": ");
	    				System.out.println("\t\t\t\t "+line);
	        	 	}
	        	 	ans.add(line);
        			line=reader.readLine();
	        	 	i++;
	         }
		}
		catch(Exception e)
		{
			System.out.println("Unable to Open File in Write Mode");
		}
		return ans;
	}
	
	public void disp()
	{
		ArrayList<String> str1 = this.getArr();
		for(String a : str1)
		{
			System.out.println(a);
		}
	}
	public boolean isLabel(String[] list) 									//function to check if a line contains a LABEL
	{
		boolean ans = false;
		String test = list[0];
		if(list.length == 1 &&  !test.contains(":"))
		{
			return ans;
		}
		else if(list.length==1 && test.contains(":"))
		{
			return true;
		}
		if(test.charAt(test.length()-1) == ':')
		{
			ans = true;
		}
		return ans;
	}
	public boolean isVariable(String[] list) 								//function to check if a line contains VARIABLE
	{
		boolean ans = false;
		if(list.length == 1)
		{
			return ans;
		}
		String test = list[1];
		if(test.equals("DS") || test.equals("DC") || test.equals("DW")) 
		{
			ans = true;
		}
		return ans;
	}
	public void writeLabel(String[] list)									//If a line contains LABEL then this function adds it to SYMBOL TABLE
	{
		String[] ans = new String[5];
		String str = list[0].substring(0, list[0].length()-1);
		ans[0] = str;
		ans[1] = "Label";
		ans[2] = Long.toString(ilc);
		ans[3] = null;
		ans[4] = null;
		table.add(ans);
	}
	public void writeVariable(String[] list)								//If a line contains VARIABLE then this function adds it to SYMBOL TABLE
	{
		String[] ans = new String[5];
		ans[0] = list[0];
		ans[1] = "Variable";
		ans[2] = Long.toString(ilc);
		ans[3] = list[2];
		if(list[1].equals("DC"))
			ans[4] = "Constant";
		if(list[1].equals("DS") || list[1].equals("DW"))
			ans[4] = "Word";
		table.add(ans);
	}
	public void create_symbol_table() 										//function to create symbol table
	{
		ArrayList<String> str = this.getArr();
		String[] list=new String[10];
		String[] h=new String[1];
		if(SYMERROR==false)
		{
		for(String a : str) 
		{
			if(a.contains("\t"))
				list = a.split("\t");
			else
			{
				h[0]=a;
				list = h;
			}
			if(isLabel(list))
			{
				writeLabel(list);
			}
			if(isVariable(list))
			{
				writeVariable(list);
			}
			ilc++;
		}
		}
	}
	public void CheckError(String[] list) throws DuplicateLabelSymbolExeption, DuplicateVariableSymbolExeption, LabelAndOpcodeHaveSameName, VariableAndOpcodeHaveSameName			//function to check for duplicate symbols while creating symbol table
	{
		if(isLabel(list))
		{
			if(opcode.contains(list[0].substring(0, list[0].length()-1)))
				throw(new LabelAndOpcodeHaveSameName());
			if(label.size()==0)
			{
				label.add(list[0]);
			}
			else
			{
				if(label.indexOf(list[0])==-1)
				{
					label.add(list[0]);
				}
				else
				{
					throw(new DuplicateLabelSymbolExeption());
				}
			}
		}
		if(isVariable(list))
		{
			if(opcode.contains(list[0]))
				throw(new VariableAndOpcodeHaveSameName());
			if(variable.size()==0)
			{
				variable.add(list[0]);
			}
			else
			{
				if(variable.indexOf(list[0])==-1)
				{
					variable.add(list[0]);
				}
				else
				{
					throw(new DuplicateVariableSymbolExeption());
				}
			}
		}
		for(String arr : list)
		{
			if(opcode.indexOf(arr)==-1)
			{
				
			}
			
		}		
	}
	public void CheckSymbolError(String[] list) throws SymbolNotFound,InvalidStatement						//function to check for undefined VARIABLES and LABELS
	{
		boolean req=false;
		boolean req2=false;
		boolean req3=false;
		for(int i=0;i<list.length;i++)
		{
			if((list[i].contains("CLA") || list[i].contains("STP")) && list.length>1)
			{
				req2=true;
			}
			if(list[i].contains(":"))
			{
				req=true;
			}
			if(list[i].contains("BRN") || list[i].contains("BRP") || list[i].contains("BRZ"))
			{
				if(!label.contains(list[i+1]+":"))
					throw(new SymbolNotFound());
			}
		}
		for(String test : list)
		{
			if(!test.contains(":") && label.contains(test+":"))
			{
				test=test+":";
			}
			if(!Pattern.compile("[0-9]").matcher(test).matches() && label.indexOf(test)==-1 && variable.indexOf(test)==-1 && opcode.indexOf(test)==-1)
			{
				if(opcode.contains(test.substring(0, test.length()-1)) && req3==false)
					return;
				if(req==true && list.length==2)
					return;
				if(req2==true && req==true)
					return;
				else if(req2==true)
						return;
				if(!Pattern.compile(".{2}[0-9]{1,3}.{1}").matcher(test).matches())
					throw(new SymbolNotFound());
			}
		}
	}
	public boolean ShowError()									//function to document errors catched on console
	{
		ArrayList<String> str = this.getArr();
		int count = 0;
		boolean ans = false;
		String[] list;
		for(String arr : str)
		{
			try 
			{
				CheckError(arr.split("\t"));
			}
			catch (DuplicateLabelSymbolExeption e) 
			{
				SYMERROR=true;
				System.out.println(e.getMessage()+" AT LINE "+count+": ");
				System.out.println("\t\t\t\t "+arr);
				ans = true;
			} 
			catch (DuplicateVariableSymbolExeption e) 
			{
				SYMERROR=true;
				System.out.println(e.getMessage()+" AT LINE "+count+": ");
				System.out.println("\t\t\t\t "+arr);
				ans = true;
			}
			catch(LabelAndOpcodeHaveSameName e)
			{
				SYMERROR=true;
				System.out.println(e.getMessage()+" AT LINE "+count+": ");
				System.out.println("\t\t\t\t "+arr);
				ans = true;
			}
			catch(VariableAndOpcodeHaveSameName e)
			{
				SYMERROR=true;
				System.out.println(e.getMessage()+" AT LINE "+count+": ");
				System.out.println("\t\t\t\t "+arr);
				ans = true;
			}
			count++;
		}
		count=0;
		for(String arr : str)
		{
			try
			{
				CheckSymbolError(arr.split("\t"));
			}
			catch (SymbolNotFound e) 
			{
				if(arr!="")
				{
					SYMERROR=true;
					System.out.println(e.getMessage()+" AT LINE "+count+": ");
					System.out.println("\t\t\t\t "+arr);
				}
				ans = true;
			}
			catch(InvalidStatement e)
			{
				SYMERROR=true;
				System.out.println(e.getMessage()+" AT LINE "+count+": ");
				System.out.println("\t\t\t\t "+arr);
			}
			count++;
		}
		int value=0;
		int index=0;
		try
		{
			String str1="";
			boolean f=false;
			for(int i=0;i<label.size();i++)
			{
				for(int j=0;j<variable.size();j++)
				{
					str1=variable.get(j)+":";
					if(label.get(i).equals(str1))
					{
						f=true;
						value=j;
						throw(new LabelVariableSameException());
					}	
				}
			}	
		}
		catch(LabelVariableSameException e)
		{
			SYMERROR=true;
			System.out.println(e.getMessage()+" AT LINES");
			for(String arr:this.getArr())
			{
				if(arr.contains(variable.get(value)) && MainCode.countspaces(arr)==2)
				{
					System.out.println(arr);
				}
			}
		}
		return ans;
	}
}
public class SymbolTable								//Marker class
{
	
}