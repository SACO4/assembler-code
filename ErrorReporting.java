class DuplicateVariableSymbolExeption extends Exception 						//In case DUPLICATE VARIABLES are present in the file
{
	public DuplicateVariableSymbolExeption()
	{
		super("DUPLICATE VARIABLE EXCEPTION");
	}
}
class DuplicateLabelSymbolExeption extends Exception 							//In case DUPLICATE LABELS are present in the file
{
	public DuplicateLabelSymbolExeption()
	{
		super("DUPLICATE LABEL EXCEPTION");
	}
}
class LabelVariableSameException extends Exception								//In case a LABEL and a VARIABLE have same name 
{
	public LabelVariableSameException()
	{
		super("SAME VARIABLE AND LABEL NAME");
	}
}
class SymbolNotFound extends Exception 											//In case symbol is UNDEFINED OR NOT FOUND ANYWHERE
{
	public SymbolNotFound()
	{
		super("SYMBOL NOT FOUND");
	}
}
class InvalidOpcodeException extends Exception									//In case OPCODE used is not a VALID OPCODE
{
	public InvalidOpcodeException()
	{
		super("OPCODE USED IS NOT A VALID OPCODE");
	}
}
class InsufficientOperands extends Exception									//In case OPCODE is not provided with SUFFICIENT number of OPERANDS
{
	public InsufficientOperands()
	{
		super("OPCODE IS NOT PROVIDED WITH REQUIRED NUMBER OF OPERANDS");
	}
}
class OpcodeHasMultipleOperands extends Exception								//In case OPCODE has been provided with EXTRA OPERANDS than REQUIRED
{
	public OpcodeHasMultipleOperands()
	{
		super("OPCODE IS PROVIDED WITH TOO MANY OPERANDS");
	}
}
class EndOfCodeMissing extends Exception										//In case STP or END OF CODE STATEMENT is MISSING
{
	public EndOfCodeMissing()
	{
		super("END STATEMENT IS MISSING");
	}
}
class InvalidStatement extends Exception										//In case the statement doesn't makes sense or there is inappropriate use of OPCODES/LITERALS
{
	public InvalidStatement()
	{
		super("THE STATEMENT IS INVALID");
	}
}
class MoreThanThreeFields extends Exception										//In case the statement has more than 3 columns/entries in assembly code
{
	public MoreThanThreeFields()
	{
		super("MORE THAN THREE FIELDS ARE PRESENT");
	}
}
class LabelAndOpcodeHaveSameName extends Exception								//In case a label has a name same as that of a valid opcode
{
	public LabelAndOpcodeHaveSameName()
	{
		super("LABEL NAME CAN'T BE THAT OF A VALID OPOCDE");
	}
}
class VariableAndOpcodeHaveSameName extends Exception							//In case a variable has a name same as that of a valid opcode
{
	public VariableAndOpcodeHaveSameName()
	{
		super("VARIABLE NAME CAN'T BE THAT OF A VALID OPOCDE");
	}
}
class CommentNotClosedException extends Exception								//In case a multi line comment has not been closed
{
	public CommentNotClosedException()
	{
		super("MULTILINE COMMENT IS NOT CLOSED");
	}
}
public class ErrorReporting {													//Marker Class


}
