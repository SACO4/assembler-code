import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
class OPTAB																	//class for making OPCODE TABLE
{
	private String OPCODE;
	private String MacCode;													//Machine Code of corresponding opcode
	private String OPERAND;
	private int InstrLength;											
	private int InstrClass;
	public OPTAB()
	{
		OPCODE="";
		MacCode="";
		OPERAND="";
		InstrLength=1;														//Instruction length is 1 word for every instruction. Assumption 1 word=12 bits.
		InstrClass=0;														//Initially zero but different for every opcode
	}
	public String getOPCODE() {
		return OPCODE;
	}
	public void setOPCODE(String oPCODE) {
		OPCODE = oPCODE;
	}
	public String getMacCode() {
		return MacCode;
	}
	public void setMacCode(String macCode) {
		MacCode = macCode;
	}
	public String getOPERAND() {
		return OPERAND;
	}
	public void setOPERAND(String oPERAND) {
		OPERAND = oPERAND;
	}
	public int getInstrLength() {
		return InstrLength;
	}
	public void setInstrLength(int instrLength) {
		InstrLength = instrLength;
	}
	public int getInstrClass() {
		return InstrClass;
	}
	public void setInstrClass(int instrClass) {
		InstrClass = instrClass;
	}
	
}
class LITERALTAB															//class for literal table
{
	private String symbol;
	private int value;
	private String TYPE;
	private int ILC;
	private String MachineValue;											//Binary value for every literal or constant
	public LITERALTAB()
	{
		symbol="";
		value=0;
		TYPE="";
		ILC=0;
		MachineValue="";
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getTYPE() {
		return TYPE;
	}
	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}
	public int getILC() {
		return ILC;
	}
	public void setILC(int iLC) {
		ILC = iLC;
	}
	public String getMachineValue() {
		return MachineValue;
	}
	public void setMachineValue(String machineValue) {
		MachineValue = machineValue;
	}
	
}
public class MainCode 
{
	static boolean ENDfound=false;
	static boolean errorfound=false;
	static boolean literror=false;
	public static ArrayList<Byte> GetData()
	{
		File file = new File("input.txt");
		ArrayList<Byte> arr = new ArrayList<Byte>();
		FileInputStream obj;
		try 
		{
			obj = new FileInputStream(file);
			byte k = 0;
			while(k!=-1)
			{
				try 
				{
					k = (byte)obj.read();
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
				arr.add(k);
			}
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return arr;
	}
	public static String SingleLineComment() 
	{
		String ans = "";
		int k = 0;
		ArrayList<Byte> arr = GetData();
		while(arr.get(k)!=-1)
		{
			if(arr.get(k)==59)
			{
				while(arr.get(k)!=10)
				{
					k++;
				}
			}
			byte s = arr.get(k);
			ans = ans + (char)s;
			k++;
		}
		return ans ;
		
	}
	public static String MultiLineComment() throws CommentNotClosedException 
	{
		String ans = "";
		int k = 0;
		int i;
		ArrayList<Byte> arr = GetData();
		while(arr.get(k)!=-1)
		{
			i=0;
			boolean flag=false;
			if(arr.get(k)==59 && arr.get(k+1)==59)
			{
				arr.remove(k);
				arr.remove(k);
				while(arr.get(k)!=59 && arr.get(k+1)!=59)
				{
					if(arr.get(k+1)==-1)
						throw(new CommentNotClosedException());
					arr.remove(k);
				}
				arr.remove(k);
				arr.remove(k);
				arr.remove(k);
				if(arr.get(k)==-1)
					break;
				if(arr.get(k)==10)
					arr.remove(k);
				arr.remove(k);
				arr.remove(k);
			}
			if(arr.get(k)==59)
			{
				if(arr.get(k-1)==10)
					flag=true;
				arr.remove(k);
				while(arr.get(k)!=10)
				{
					arr.remove(k);
				}
				if(flag==false)
					arr.add(k, (byte)13);
				arr.remove(k-1);
			}
			k++;
		}
		k = 0;
		while(arr.get(k)!=-1)
		{
			byte s = arr.get(k);
			ans = ans + (char)s;
			k++;
		}
		return ans ;
	}
	public static void Write(String str) throws IOException
	{
		byte arr[] = str.getBytes();
		File file = new File("finalinput.txt");
		try 
		{
			FileOutputStream fo = new FileOutputStream(file);
			for(byte a : arr)
			{
				try 
				{
					fo.write(a);
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			fo.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	public static int countspaces(String line)								//function to count number of tab spaces in assembly code in each line
	{
		int count=0;
		for(int i=0;i<line.length();i++)
		{
			if(Character.toString(line.charAt(i)).equals("\t"))
			{
				count++;
			}
		}
		return count;
	}
	//function to add encountered LITERAL to LITERAL TABLE
	public static void Enter_to_LITERALTAB(ArrayList<LITERALTAB> literals,String line,int index) throws InvalidStatement
	{
		String[] arr;
		String str;
		int spaces=countspaces(line);
		if(line.contains("=") && (line.contains("SAC") || line.contains("BRZ") || line.contains("BRN") || line.contains("BRP") || line.contains("INP") || line.contains("DSP")))
		{
			literror=true;
			throw(new InvalidStatement());									//SAC,BRZ,BRN,BRP,INP,DSP can't have literals as their operands
		}
		if(line.contains("=") && (spaces==1 || spaces==2))
		{
			arr=line.split("\t");
			if(spaces==1)
			{
				if(!(arr[0].equals("SAC") || arr[0].equals("BRZ") || arr[0].equals("BRN") || arr[0].equals("BRP") || arr[0].equals("INP") || arr[0].equals("DSP")))
				{
					if(Pattern.compile(".{2}[a-zA-Z0-9]{1,3}.{1}").matcher(arr[1]).matches())
					{
					int i=arr[1].indexOf("=");
					literals.get(index).setSymbol(arr[1]);
					literals.get(index).setTYPE("Literal");
					literals.get(index).setValue(Integer.parseInt(arr[1].substring(i+1, arr[1].length()-1)));
					literals.get(index).setILC(index);
					str=Integer.toBinaryString(literals.get(index).getValue());
					int l=str.length();
					for(int k=0;k<8-l;k++)
					{
						str="0"+str;
					}
					literals.get(index).setMachineValue(str);					//8 bit machine value of literals
				}
				}
				else
					throw(new InvalidStatement());
			}
			else
			{
				if(!(arr[1].equals("SAC") || arr[1].equals("BRZ") || arr[1].equals("BRN") || arr[1].equals("BRP") || arr[1].equals("INP") || arr[1].equals("DSP")))
				{
					if(Pattern.compile(".{2}[a-zA-Z0-9]{1,3}.{1}").matcher(arr[2]).matches())
					{
					int i=arr[2].indexOf("=");
					literals.get(index).setSymbol(arr[2]);
					literals.get(index).setTYPE("Literal");
					literals.get(index).setValue(Integer.parseInt(arr[2].substring(i+1, arr[2].length()-1)));
					literals.get(index).setILC(index);
					str=Integer.toBinaryString(literals.get(index).getValue());
					int l=str.length();
					for(int k=0;k<8-l;k++)
					{
						str="0"+str;
					}
					literals.get(index).setMachineValue(str);					//8 bit machine value of literals
				}
					else
						throw(new InvalidStatement());
				}
			}
		}
	}
	//Function to add encountered OPCODE in OPCODE TABLE
	public static void Enter_to_OPTAB(ArrayList<OPTAB> opcodetable,String line,int index,HashMap hm,HashMap hc) throws InvalidOpcodeException,OpcodeHasMultipleOperands,InsufficientOperands,InvalidStatement
	{
		
		String[] arr;
		boolean flag=false;
		boolean errorOPCODE=false;
		if(line.contains("STP"))
		{
			ENDfound=true;													//If ENDfound=false at the end then END statement is missing in the code
		}
		int spaces=countspaces(line);
		if(spaces==0 && !line.contains(":"))
		{
			if(!hm.keySet().contains(line))
				throw (new InvalidOpcodeException());
			if(!(line.equals("CLA") || line.equals("STP")))					//CLA and STP don't require operands
			{
				throw (new InsufficientOperands());
			}
			opcodetable.get(index).setOPCODE(line);
			opcodetable.get(index).setMacCode((String)hm.get(line));
			opcodetable.get(index).setInstrClass((int)hc.get(line));
		}
		else
		{
			arr=line.split("\t");
			if(spaces==1)
			{
				if(arr[0].contains(":"))
				{
					if(!(hm.keySet().contains(arr[1])))
					{
						throw (new InvalidOpcodeException());
					}
					if(arr.length==2 &&	!(arr[1].equals("CLA") || arr[1].equals("STP")) )
					{
						throw (new InsufficientOperands());
					}
					opcodetable.get(index).setOPCODE(arr[1]);
					opcodetable.get(index).setMacCode((String)hm.get(arr[1]));
					opcodetable.get(index).setInstrClass((int)hc.get(arr[1]));
				}
				else
				{
				if(!(hm.keySet().contains(arr[0])))
					throw (new InvalidOpcodeException());
				if(arr.length==1 &&	!(arr[0].equals("CLA") || arr[0].equals("STP")) )
				{
					throw (new InsufficientOperands());
				}
				if((arr[0].equals("STP") || arr[0].equals("CLA")) && arr.length>=2)
					throw(new InvalidStatement());
				if(arr[1].contains(","))
					throw (new OpcodeHasMultipleOperands());
				if((arr[0].equals("BRN") || arr[0].equals("BRP") || arr[0].equals("BRZ")))
				{
					if(arr[1].matches("-?\\d+(\\.\\d+)?"))
					throw(new InvalidStatement());
				}
				if(!(arr[0].equals("BRN") || arr[0].equals("BRP") || arr[0].equals("BRZ")))
				{
					if(symbol.label.contains(arr[1]+":"))
					{
						throw(new InvalidStatement());
					}
				}
				opcodetable.get(index).setOPCODE(arr[0]);
				opcodetable.get(index).setOPERAND(arr[1]);
				opcodetable.get(index).setMacCode((String)hm.get(arr[0]));
				opcodetable.get(index).setInstrClass((int)hc.get(arr[0]));
				}
			}
			else if(spaces==2 && line.contains(":"))
			{
				if(arr[1].equals("DS") || arr[1].equals("DC") || arr[1].equals("DW"))
				{
					if(arr[0].contains(":"))
					{
						throw(new InvalidStatement());
					}
				}
				if(!hm.keySet().contains(arr[1]))
					throw (new InvalidOpcodeException());
				if(arr[2].contains(","))
					throw (new OpcodeHasMultipleOperands());
				if((arr[1].equals("STP") || arr[1].equals("CLA")) && arr.length>=2)
					throw(new InvalidStatement());
				if((arr[1].equals("BRN") || arr[1].equals("BRP") || arr[1].equals("BRZ")))
				{
					if(arr[2].matches("-?\\d+(\\.\\d+)?"))
					throw(new InvalidStatement());
				}
				if(!(arr[1].equals("BRN") || arr[1].equals("BRP") || arr[1].equals("BRZ")))
				{
					if(symbol.label.contains(arr[2]+":"))
					{
						throw(new InvalidStatement());
					}
				}
				opcodetable.get(index).setOPCODE(arr[1]);
				opcodetable.get(index).setOPERAND(arr[2]);
				opcodetable.get(index).setMacCode((String)hm.get(arr[1]));
				opcodetable.get(index).setInstrClass((int)hc.get(arr[1]));
			}
		}
	}
	public static void Display_LITERALS(ArrayList<LITERALTAB> literals)
	{
		System.out.println("\t\t\t\t\tLITERAL TABLE");
		System.out.println("------------------------------------------------------------------------------");
		System.out.println("SYMBOL"+"\t\t"+"TYPE"+"\t\t"+"VALUE"+"\t\t"+"MACHINE VALUE"+"\t\t"+"ILC");
		System.out.println("------------------------------------------------------------------------------");
		for(int j=0;j<literals.size();j++)
		{
			if(literals.get(j).getSymbol()!="")
			{
				System.out.println(literals.get(j).getSymbol()+"\t\t"+literals.get(j).getTYPE()+"\t\t"+literals.get(j).getValue()+"\t\t"+literals.get(j).getMachineValue()+"\t\t"+literals.get(j).getILC());
			}
		}
	}
	public static void Display_OPCODES(ArrayList<OPTAB> opcodetable)
	{
		System.out.println("\t\t\t\t\tOPCODE TABLE");
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		System.out.println("OPCODE"+"\t\t"+"MACHINE CODE"+"\t\t"+"OPERAND"+"\t\t"+"INSTRUCTION LENGTH"+"\t\t"+"INSTRUCTION CLASS");
		System.out.println("-----------------------------------------------------------------------------------------------------------");
		for(int j=0;j<opcodetable.size();j++)
		{
			if(opcodetable.get(j).getOPCODE()!="")
			{
				System.out.println(opcodetable.get(j).getOPCODE()+"\t\t"+opcodetable.get(j).getMacCode()+"\t\t\t"+opcodetable.get(j).getOPERAND()+"\t\t\t"+opcodetable.get(j).getInstrLength()+"\t\t\t\t"+opcodetable.get(j).getInstrClass());
			}
		}
	}
	//function to read literals present line by line from input file
	public static ArrayList<LITERALTAB> literal()
	{
		ArrayList<LITERALTAB> literals=new ArrayList<LITERALTAB>();
		for(int i=0;i<100;i++)
		{
			LITERALTAB p=new LITERALTAB();
			literals.add(p);
		}
		BufferedReader reader;
		try
		{ 
			FileInputStream fstream = new FileInputStream("finalinput.txt");
			reader = new BufferedReader(new InputStreamReader(fstream));
	         String line=reader.readLine();
	         int i=0;
	         while(line!=null)
	         {
	        	 try {
	        	 Enter_to_LITERALTAB(literals,line,i);
	        	 }
	        	 catch(InvalidStatement e)
	        	 {
	        		 System.out.println(e.getMessage()+" AT LINE "+i+": ");
	        		 System.out.println("\t\t\t\t "+line);
	        	 }
	        	 line=reader.readLine();
	        	 i++;
	         }
		}
		catch(FileNotFoundException E)
		{
			System.out.println(E.getMessage());
		}
		catch(IOException E)
		{
			System.out.println(E.getMessage());
		}
		if(literror==false && symbol_table.SYMERROR==false)
		{
		//LITERAL TABLE formatting done to be printed on console
			Display_LITERALS(literals);
		}
		else
			literals.clear();
		return literals;
	}
	//function to read opcodes line by line from input file
	public static ArrayList<OPTAB> opcode()
	{
		BufferedReader reader;
		ArrayList<OPTAB> opcodetable=new ArrayList<OPTAB>();
		HashMap<String,String> hm=new HashMap<String,String>();
		HashMap<String,Integer> hc=new HashMap<String,Integer>();
		hm.put("CLA","0000");
		hm.put("LAC","0001");
		hm.put("SAC","0010");
		hm.put("ADD","0011");
		hm.put("SUB","0100");
		hm.put("BRZ","0101");
		hm.put("BRN","0110");
		hm.put("BRP","0111");
		hm.put("INP","1000");
		hm.put("DSP","1001");
		hm.put("MUL","1010");
		hm.put("DIV","1011");
		hm.put("STP","1100");
		hc.put("CLA",0);
		hc.put("LAC",1);
		hc.put("SAC",2);
		hc.put("ADD",3);
		hc.put("SUB",4);
		hc.put("BRZ",5);
		hc.put("BRN",6);
		hc.put("BRP",7);
		hc.put("INP",8);
		hc.put("DSP",9);
		hc.put("MUL",10);
		hc.put("DIV",11);
		hc.put("STP",12);
		for(int i=0;i<100;i++)
		{
			OPTAB p=new OPTAB();
			opcodetable.add(p);
		}
		try
		{ 
			FileInputStream fstream = new FileInputStream("finalinput.txt");
			reader = new BufferedReader(new InputStreamReader(fstream));
	         String line=reader.readLine();
	         int i=0;
	         while(line!=null)
	         {
	        	 try
	        	 {
	        	 Enter_to_OPTAB(opcodetable,line,i,hm,hc);
	        	 }
	        	 catch(InvalidOpcodeException e)
	        	 {
	        		 errorfound=true;
	        		 System.out.println(e.getMessage()+" AT LINE "+i+": ");
	        		 System.out.println("\t\t\t\t\t "+line);
	        	 }
	        	 catch(OpcodeHasMultipleOperands e)
	        	 {
	        		 errorfound=true;
	        		System.out.println(e.getMessage()+" AT LINE "+i+": "); 
	        		System.out.println("\t\t\t\t\t "+line);
	        	 }
	        	 catch(InsufficientOperands e)
	        	 {
	        		errorfound=true;
	        		System.out.println(e.getMessage()+" AT LINE "+i+": ");
	        		System.out.println("\t\t\t\t\t "+line		);
	        	 }
	        	 catch(InvalidStatement e)
	        	 {
	        			errorfound=true;
		        		System.out.println(e.getMessage()+" AT LINE "+i+": ");
		        		System.out.println("\t\t\t\t\t "+line		);
	        	 }
	        	 line=reader.readLine();
	        	 i++;
	         }
		}
		catch(FileNotFoundException E)
		{
			System.out.println(E.getMessage());
		}
		catch(IOException E)
		{
			System.out.println(E.getMessage());
		}
		if(ENDfound==false)
		{
			errorfound=true;
			EndOfCodeMissing E=new EndOfCodeMissing();
			System.out.println(E.getMessage());
		}
		if(errorfound==false && literror==false && symbol_table.SYMERROR==false)
		{
		//OPCODE TABLE formatting done to be printed on console
			Display_OPCODES(opcodetable);
		}
		else
			opcodetable.clear();
		return opcodetable;
	}
	//function for making symbol table
	public static ArrayList<String[]> symbol() 
	{
		symbol_table obj = new symbol_table();
		obj.create_symbol_table();											//function to create symbol table whose code is defined in another file
		if(obj.ShowError()==false && obj.SYMERROR==false)
		{
			obj.display_table();
		}
		else
		{
			obj.table.clear();
		}
		return obj.table;
	}
	//Function to convert error free assembly code to machine code
	public static void ConvertToMacCode(ArrayList<String[]> symboltable,ArrayList<OPTAB> opcodetable,ArrayList<LITERALTAB> literaltable)
	{
		BufferedReader reader;
		try
		{ 
			FileInputStream fstream = new FileInputStream("finalinput.txt");
			FileWriter fostream=new FileWriter("output.txt");
			reader = new BufferedReader(new InputStreamReader(fstream));
			BufferedWriter writer;
			writer=new BufferedWriter(fostream);
	         String line=reader.readLine();
	         int i=0;
	         String[] arr=new String[3];
	         int spaces=0;
	         System.out.println();
	         while(line!=null)
	         {
	        	 for(int j=0;j<opcodetable.size();j++)
	        	 {
	        		 int flag=0;
	        		 boolean f1=false;
	        		 boolean f2=false;
	        		 if(line.contains(opcodetable.get(j).getOPCODE()))
	        		 {
	        			 spaces=countspaces(line);
	        			 arr=line.split("\t");
	        			 for(int h=0;h<arr.length;h++)
	        			 {
	        				 if(arr[h].equals(opcodetable.get(j).getOPCODE()))
	        					 f1=true;
	        			 }
	        			 if(f1==true)
	        			 {
	        			 if(spaces==1)
	        			 {
	        				 boolean insymbol=false;
	        				 for(String[] array : symboltable)
	        				 {
	        					 if(array[0].equals(arr[1]))
	        					 {
	        						 insymbol=true;
	        						 int z=Integer.parseInt(array[2]);
	        						 String str=Integer.toBinaryString(z);
	        						 int l=str.length();
	        						 for(int p=0;p<8-l;p++)
	        						 {
	        							str="0"+str;
	        						 }
	        						 writer.write(opcodetable.get(j).getMacCode()+"\t"+str);
	        						 writer.newLine();
	        						 flag=1;
	        						 break;
	        					 }
	        				 }
	        				 if(insymbol==false)
	        				 {
	        					 for(int k=0;k<literaltable.size();k++)
	        					 {
	        						 if(literaltable.get(k).getSymbol().equals(arr[1]))
	        						 {
	        							 writer.write(opcodetable.get(j).getMacCode()+"\t"+literaltable.get(k).getMachineValue());
	        							 writer.newLine();
	        							 flag=1;
	        							 break;
	        						 }
	        					 }
	        				 }
	        				 if(arr[1].equals("STP"))
	        				 {
	        					 writer.write(opcodetable.get(j).getMacCode());
		        				 writer.newLine();
	        				 }
	        			 }
	        			 else if(spaces==2)
	        			 {
	        				 boolean insymbol=false;
	        				 for(String[] array : symboltable)
	        				 {
	        					 if(array[0].equals(arr[2]))
	        					 {
	        						 insymbol=true;
	        						 int z=Integer.parseInt(array[2]);
	        						 String str=Integer.toBinaryString(z);
	        						 int l=str.length();
	        						 for(int p=0;p<8-l;p++)
	        						 {
	        							str="0"+str;
	        						 }
	        						 writer.write(opcodetable.get(j).getMacCode()+"\t"+str);
	        						 writer.newLine();
	        						 flag=1;
	        						 break;
	        					 }
	        				 }
	        				 if(insymbol==false)
	        				 {
	        					 for(int k=0;k<literaltable.size();k++)
	        					 {
	        						 if(literaltable.get(k).getSymbol().equals(arr[2]))
	        						 {
	        							 writer.write(opcodetable.get(j).getMacCode()+"\t"+literaltable.get(k).getMachineValue());
	        							 writer.newLine();
	        							 flag=1;
	        							 break;
	        						 }
	        					 }
	        				 }
	        			 }
	        			 else
	        			 {
	        				 writer.write(opcodetable.get(j).getMacCode());
	        				 writer.newLine();
	        				 flag=1;
	        				 break;
	        			 }
	        		 }
	        		 if(flag==1)
	        		 {
	        			 break;
	        		 }
	        	 }
	        	 }
	        	 line=reader.readLine();
	        	 i++;
	         }
	         writer.close();
	         reader.close();
		}
		catch(FileNotFoundException E)
		{
			System.out.println(E.getMessage());
		}
		catch(IOException E)
		{
			System.out.println(E.getMessage());
		}
	}
	public static void main(String args[]) throws SymbolNotFound, DuplicateLabelSymbolExeption, DuplicateVariableSymbolExeption, IOException
	{
		ArrayList<String[]> symboltable=new ArrayList<String[]>();
		ArrayList<OPTAB> opcodetable=new ArrayList<OPTAB>();
		ArrayList<LITERALTAB> literaltable=new ArrayList<LITERALTAB>();
		String str="";
		try
		{
		str = MultiLineComment();
		}
		catch(CommentNotClosedException e)
		{
			System.out.println(e.getMessage());
		}
		Write(str);
		symboltable=symbol();
		System.out.println();
		System.out.println();
		literaltable=literal();
		System.out.println();
		System.out.println();
		opcodetable=opcode();
		ConvertToMacCode(symboltable,opcodetable,literaltable);
	}
}
